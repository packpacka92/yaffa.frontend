import { get } from '../utils/api'; 

export function getFeatures() {
    return get('/features');
}

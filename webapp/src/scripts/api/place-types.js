import { get } from '../utils/api'; 

export function getPlaceTypes() {
    return get('/placetypes');
}

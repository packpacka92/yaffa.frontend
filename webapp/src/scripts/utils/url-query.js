import queryString from 'query-string';
import { omitBy } from 'lodash/object';
import { forEach } from 'lodash/collection';

function normalizeParams(params) {
    let compactParams = omitBy(params, function (value, key) {
        return value === '' || value === undefined || value === null;
    }, {});
    return compactParams;
}



export function parseQuery(paramsString) {
    let parsed = queryString.parse(paramsString);
    return parsed;
}

export function buildQuery(object, options) {
    let query = queryString.stringify(normalizeParams(object), options);
    return !query ? '' : '?' + query;
}
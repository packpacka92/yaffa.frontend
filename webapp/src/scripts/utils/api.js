import * as http from './http';

import { assign } from 'lodash/object';

export const get = wrapHttp(http.get);

function wrapHttp(method) {
    return function(url, params = {}) {
        var apiUrl = 'http://46.101.197.208/api' + url;
        var apiParams = assign({
            headers: {
                'Accept': 'application/json',
                'Accept-Charset': 'utf-8',
                'Origin': 'www.test.ru',
                'Accept-Encoding': 'gzip, deflate, sdch'
            },
            redirect: 'follow',
            mode: 'cors'
        }, params);
        return method(apiUrl, apiParams);
    };
}

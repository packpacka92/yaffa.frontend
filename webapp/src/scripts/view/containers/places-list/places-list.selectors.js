import { searchParamsSelector } from '../search-panel/search-panel.selectors';
import { createSelector } from 'reselect';

export const foundPlacesSelector =  state => state.placesList;

function placesListSelector(state = {}) {
    return {
        placesList: foundPlacesSelector(state),
        search: searchParamsSelector(state)
    };
}

export default placesListSelector;
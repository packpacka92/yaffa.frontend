import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPlaces } from '../../../redux/places/places.actions';
import { bindActionCreators } from 'redux';
import PlaceListItem from './places-list-item.jsx';
import placesListSelector from './places-list.selectors';


class PlacesList extends Component {
    componentDidMount() {
        this.props.fetchPlaces(this.props.search);
    }
    render() {
        var places = this.props.placesList.places;
        return (
            <div className="places-list flexbox-row">
                {places.map(function(place) {
                    return (
                        <div className="places-list__item col-xs-12 col-sm-6 col-lg-4">
                            <PlaceListItem place={place}/>
                        </div>
                    );
                })}
            </div>
        );
    }
}

function mapDispatch(dispatch) {
    return bindActionCreators({ fetchPlaces }, dispatch);
}

export default connect(placesListSelector, mapDispatch)(PlacesList);
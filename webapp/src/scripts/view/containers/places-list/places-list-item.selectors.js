import { createSelector } from 'reselect';
import { filter } from 'lodash/collection';
import { searchParamsSelector } from '../search-panel/search-panel.selectors';

const currentSearchParamsSelector = state => state.currentSearch;
const placeSelector = (state, props) => props.place;
const makeShowedFeaturesSelector = () => createSelector([currentSearchParamsSelector, placeSelector], filterPlaceFeatures);

function filterPlaceFeatures(search, place) {
    console.log(place.name);
    return filter(place.features, (feature) => {
        return search.features.indexOf(feature.feature_id) > -1;
    });
}

function placesListItemSelector() {
    return (state = {}, props = {}) => {
        return {
            search: currentSearchParamsSelector(state),
            showedTags: makeShowedFeaturesSelector()(state, props)
        };
    };
}

export default placesListItemSelector;
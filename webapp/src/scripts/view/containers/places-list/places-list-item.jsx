import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUrl } from '../../services/navigator';
import placesListSelector from './places-list-item.selectors';


class PlaceListItem extends Component {
    getDetailsUrl() {
        return getUrl('place-details', { placeId: this.props.place.id });
    }
    renderTags() {
        return this.props.showedTags.map((tag) => (
            <div className="place__feature">
                <span className="place__feature-name">{tag.name}</span>
                <span className="place__feature-description">{tag.description}</span>
            </div>
        ));
    }
    render() {
        return (
            <div className="m-card m-shadow-2">
                <div className="m-card__image m-card__image--overlay place__image">
                    <div className="place__title">
                        {this.props.place.name}
                        <div className="place__description mt-5">{this.props.place.address}</div>
                    </div>
                </div>
                <div className="m-card__content">
                    {this.renderTags() }
                </div>
                <div className="m-card__actions">
                    <a className="m-button  m-button--primary" href={this.getDetailsUrl() }>подробнее</a>
                </div>
            </div>
        );
    }
}

PlaceListItem.propTypes = {
    place: PropTypes.object.isRequired
};

function mapDispatch(dispatch) {
    return {
    };
}

export default connect(placesListSelector, mapDispatch)(PlaceListItem);



import { createSelector } from 'reselect';

export const searchPanelSlector = state => state.searchPanel;
export const searchParamsSelector = createSelector(searchPanelSlector, searchPanel => searchPanel.search);
export const searchTextSelector = createSelector([searchParamsSelector], search => search.searchText);
export const selectedFeaturesSelector = createSelector([searchParamsSelector], search => search.features);
export const selectedPlaceTypesSelector = createSelector([searchParamsSelector], search => search.placeTypes);
export const onlyNearbySelector = createSelector([searchParamsSelector], search => search.onlyNearby);
export const openedNowSelector = createSelector([searchParamsSelector], search => search.openedNow);
export const featuresListSelector = createSelector([searchPanelSlector], searchPanel => searchPanel.featuresList.features);
export const placeTypesListSelector = createSelector([searchPanelSlector], searchPanel => searchPanel.placeTypesList.placeTypes);

function searchPanelSelector(state = {}) {
    return {
        search: searchParamsSelector(state),
        searchText: searchTextSelector(state),
        selectedFeatures: selectedFeaturesSelector(state),
        selectedPlaceTypes: selectedPlaceTypesSelector(state),
        featuresList: featuresListSelector(state), 
        placeTypesList: placeTypesListSelector(state),
        onlyNearby: onlyNearbySelector(state),
        openedNow: openedNowSelector(state)
    };
}

export default searchPanelSelector;
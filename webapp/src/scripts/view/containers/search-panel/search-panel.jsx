import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect} from 'react-redux';
import { Input, Button, Switch, Select } from '../../components/controls/simple-controls.jsx';
import * as searchPanelActions from '../../../redux/search-panel/search-panel.actions';
import { fetchFeatures } from '../../../redux/features/features.actions';
import { fetchPlaceTypes } from '../../../redux/place-types/place-types.actions';
import { get } from '../../../utils/http';
import { assign } from 'lodash/object';
import { goTo } from '../../services/navigator';
import searchPanelSelector from './search-panel.selectors';

class SearchPanelClass extends Component {
    constructor() {
        super(...arguments);
        this.state = {
            detailsExpanded: false
        };
    }
    componentDidMount() {
        this.props.fetchFeatures();
        this.props.fetchPlaceTypes();
    }
    toggleDetails() { 
        this.setState({ detailsExpanded: !this.state.detailsExpanded });
    }

    getTagClass(item, list) {
        return list && list.indexOf(item) > -1 ? 'details-group__tag--selected' : '';
    }

    renderTagsList(listAll, listSelected, idField, nameField, toggler, keyPrefix) {
        return listAll.map(item => {
            let idValue = item[idField];
            let nameValue = item[nameField];
            return <span className={`details-group__tag ${this.getTagClass(idValue, listSelected)}`} key={keyPrefix + nameValue} onClick={() => toggler(item) }>{nameValue}</span>;
        });
    }
    
    runSearch(searchParams) {
        goTo('search', searchParams);
    }

    onSearchSubmit(e) {
        e.preventDefault();
        this.runSearch(this.props.search);
    }
    render() {
        var input;
        var props = this.props;

        var feature = this.renderTagsList(props.featuresList, props.selectedFeatures, 'id', 'name', props.toggleFeature, 'f');
        var placeTypes = this.renderTagsList(props.placeTypesList, props.selectedPlaceTypes, 'id', 'name', props.togglePlaceType, 't');
        
        var checkSummCategories = ['$', '$$', '$$$'].map(function(category) {
           return   <span className='details-group__tag'>{category}</span>;
        });
        
        return (
            <form className="search-panel" onSubmit={(e) => this.onSearchSubmit(e)}>
                <div className="search-panel__title search-panel__content">Куда пойдем?</div>
                <div className="search-panel__content search-block">
                    <Input className="search-block__input" value={props.searchText} ref={(node) => { input = node; } } onChange={()=> props.setSearchText(input.value())} placeholder="Введите название заведения"/>
                    <button className="search-block__button"  type="submit" >Найти</button>
                </div>
                <div className="search-panel__details search-panel__content">
                    <div className="mb-15 flexbox-row">
                        <div className="search-panel__details-group col-xs-12 col-md">
                            <Switch label="Все равно где" value={this.props.onlyNearby} ref="locationSwitch" onChange={() => props.setLocationSignificance(this.refs.locationSwitch.value())}/>
                        </div>
                        <div className="search-panel__details-group col-xs-12  col-md">
                            <Switch label="Прямо сейчас" value={this.props.openedNow} ref="openedSwitch" onChange={() => props.setWorktimeSignificance(this.refs.openedSwitch.value())}/>
                        </div>
                        <div className="search-panel__details-group col-xs-12 col-md-3">
                            <Switch label="Оплата картой" value={false} ref="cardPaymennt" onChange={() => console.log(this.refs.cardPaymennt.value())}/>
                        </div>
                    </div>
                    <div className="search-panel__details-content flexbox-row">
                        <div className="search-panel__details-group col-xs-12 col-md">
                            <div className="group-title ">
                                Хочу...
                            </div>
                            <div className="details-group__tags-block">
                                {feature}
                            </div>
                        </div>
                        <div className="group-divider col-xs-0"></div>
                        <div className="search-panel__details-group col-xs-12 col-md">
                            <div className="group-title">
                                Хочу в...
                            </div>
                            <div className="details-group__tags-block">
                                {placeTypes}
                            </div>
                        </div>
                        <div className="group-divider col-xs-0"></div>
                    </div>
                </div>
            </form>
        );
    }
}

function mapDispatchToProps(dispatch) {
    var actions = assign({}, searchPanelActions, {
        fetchFeatures,
        fetchPlaceTypes
    });
    
    return bindActionCreators(actions, dispatch);
}

const searchPanelContainer = connect(searchPanelSelector, mapDispatchToProps)(SearchPanelClass);

export default searchPanelContainer;
import React, {Component} from 'react';
/**
 * Класс реализующий базовые методы всех контроллов
 */
class AbstractControl extends Component {
    componentDidMount() {
        if (super.componentDidMount) {
            super.componentDidMount(...arguments);
        }

        if (this.props.value) {
            this.value(this.props.value);
        }
    }

    getClass() {
        var result = '';
        if (this.props.className) {
            result += this.props.className + ' ';
        }

        if (this.getInternalClass()) {
            result += this.getInternalClass();
        }
        return result;
    }
    getInternalClass() {
        return '';
    }
    value() {
        console.error(`Method "value" are not implemented for ${this.constructor.name} controll`);
    }
}

export default AbstractControl;
import React, {Component} from 'react';
import AbstractControl from './abstract-control.jsx';
import {SimpleSelect, MultiSelect} from 'react-selectize';


class Combobox extends AbstractControl {
    render() {
        var options = (this.props.options || []).map(function(fruit) {
            return { label: fruit, value: fruit };
        });
        return (<SimpleSelect
            value = {this.props.value}
            options = {options}
            placeholder = {this.props.placeholder}
            theme = "material"
            tether = {true}
            />);
    }
}

class MCombobox extends AbstractControl {
    render() {
        var options = (this.props.options || []).map(function(fruit) {
            return { label: fruit, value: fruit };
        });
        return (<MultiSelect
            values = {this.props.values}
            options = {this.props.options}
            placeholder = {this.props.placeholder}
            theme = "material"
            tether = {true}
            />);
    }
}

export {
    Combobox,
    MCombobox
};
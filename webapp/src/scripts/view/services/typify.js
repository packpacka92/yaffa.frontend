import { assign, has } from 'lodash/object';
import { isArray, isUndefined } from 'lodash/lang';

export function integer(value){
    return +value;
}
export function bool(value){
    return value === 'true' || value === true;
}

export function string(value) {
    return value? value.toString(): '';
}

export function arrayOf(type) {
    function toArray(value) {
        if(isUndefined(value)){
            return [];
        }
        if(!isArray(value)) {
            return [value];
        }
        return value;
    }
    return function (arr) {
        return toArray(arr).map(type);
    };
}

export function object(schema) {
    return function (value) {
        var result = {};
        Object.keys(schema).forEach(function (key) {
            var type = schema[key];
            result[key] = type(value[key]);
        });
        return assign({},value, result);
    };
}

export function typify(value, type) {
    return type(value);
}
import React, {Component} from 'react';
import { Provider } from 'react-redux';
import ReactDom from 'react-dom';
import { createStore } from 'redux';
import docready from '../utils/docready';
import middlewares from '../../redux/middleware/middlewares';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import Drawer from '../components/drawer.jsx';
import PlaceDetails from '../containers/place-details/place-details.jsx';
import placeReducer from '../../redux/place-details/place-details.reducers';

let store = createStore(placeReducer, middlewares);

docready(() => {
    ReactDom.render(
        <Provider store={store}>
             <div className="layout">
                <Header />
                <div className="layout-content">
                    <PlaceDetails />
                    <Footer />
                </div>
            </div>
        </Provider>,
        document.getElementById('domRoot')
    );
});

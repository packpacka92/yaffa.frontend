import {createAction} from 'redux-actions';
import { getPlaces } from '../../api/places';
import { createApiAction } from '../utils/async-actions';
import * as actionTypes from './places.action-types';


const fetchPlaces = createApiAction({
    makeRequest: getPlaces,
    actionTypes: [
        actionTypes.FETCH_PLACES,
        actionTypes.FETCH_PLACES_SUCCESS,
        actionTypes.FETCH_PLACES_FAIL,
        actionTypes.FETCH_PLACES_DONE
    ]
});

export {
    fetchPlaces
};
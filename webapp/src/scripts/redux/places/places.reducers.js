import {combineReducers} from 'redux';
import * as actionTypes from './places.action-types';
import { createReadReducer } from '../utils/crud';


const placesList = createReadReducer(
    'places',
    [
        actionTypes.FETCH_PLACES,
        actionTypes.FETCH_PLACES_SUCCESS,
        actionTypes.FETCH_PLACES_FAIL,
        actionTypes.FETCH_PLACES_DONE
    ],
    {
        places: []
    }
);

export default placesList;

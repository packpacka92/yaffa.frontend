import {createAction} from 'redux-actions';
import { getPlaceTypes } from '../../api/place-types';
import { createApiAction } from '../utils/async-actions';
import * as actionTypes from './place-types.action-types';


const fetchPlaceTypes = createApiAction({
    makeRequest: getPlaceTypes,
    actionTypes: [
        actionTypes.FETCH_PLACE_TYPES,
        actionTypes.FETCH_PLACE_TYPES_SUCCESS,
        actionTypes.FETCH_PLACE_TYPES_FAIL,
        actionTypes.FETCH_PLACE_TYPES_DONE
    ]
});

export {
    fetchPlaceTypes
};
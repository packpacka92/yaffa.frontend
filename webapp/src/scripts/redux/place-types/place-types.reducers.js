import { combineReducers } from 'redux';
import * as actionTypes from './place-types.action-types';
import { createReadReducer } from '../utils/crud';


const placeTypesList = createReadReducer(
    'placeTypes',
    [
        actionTypes.FETCH_PLACE_TYPES,
        actionTypes.FETCH_PLACE_TYPES_SUCCESS,
        actionTypes.FETCH_PLACE_TYPES_FAIL,
        actionTypes.FETCH_PLACE_TYPES_DONE
    ],
    {
        placeTypes: []
    }
);

export default placeTypesList;
import { combineReducers } from 'redux';
import searchPanel from '../search-panel/search-panel.reducers';
import placesList from '../places/places.reducers';
import { assign } from 'lodash/object';


function currentSearch(state = {}) {
    return assign({}, state);
}

export default combineReducers({
    searchPanel,
    currentSearch,
    placesList
});
import {createAction} from 'redux-actions';
import { getFeatures } from '../../api/features';
import { createApiAction } from '../utils/async-actions';
import * as actionTypes from './features.action-types';


const fetchFeatures = createApiAction({
    makeRequest: getFeatures,
    actionTypes: [
        actionTypes.FETCH_FEATURES,
        actionTypes.FETCH_FEATURES_SUCCESS,
        actionTypes.FETCH_FEATURES_FAIL,
        actionTypes.FETCH_FEATURES_DONE
    ]
});

export {
    fetchFeatures
};
import {combineReducers} from 'redux';
import * as actionTypes from './features.action-types';
import { createReadReducer } from '../utils/crud';


const featuresList = createReadReducer(
    'features',
    [
        actionTypes.FETCH_FEATURES,
        actionTypes.FETCH_FEATURES_SUCCESS,
        actionTypes.FETCH_FEATURES_FAIL,
        actionTypes.FETCH_FEATURES_DONE
    ],
    {
        features: []
    }
);

export default featuresList;
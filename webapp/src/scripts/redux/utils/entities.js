import {union} from 'lodash/array';
import {isFunction, isArray, isNumber} from 'lodash/lang';
import {mapValues, mergeWith} from 'lodash/object';
import { every } from 'lodash/collection';

function selectEntities(action, name) {
    if (action.payload && 
        action.payload && 
        action.payload.entities && 
        action.payload.entities[name]) {
        return action.payload.entities[name];
    }
}

export function entitiesReducer(reducer, entitiesName) {
    function mergeEntities(objValue, srcValue) {
        if (isArray(objValue) && every(objValue, item => isNumber(item))) {
            return union(objValue, srcValue);
        }
    }

    return (state, action) => {
        let newState = state;
        const entities = isFunction(entitiesName) ? entitiesName(action) : selectEntities(action, entitiesName);

        if (entities) {
            newState = mergeWith({}, newState, entities, mergeEntities);
        }

        return reducer(newState, action);
    };
}

export function combineEntitiesReducers(reducers) {
    const entitiesReducers = mapValues(reducers, entitiesReducer);
    return (state = {}, action) => mapValues(entitiesReducers,
        (reducer, key) => reducer(state[key], action));
}

export function emptyReducer(state = {}, action) {
    return state;
}


export function areEqual(entity, criteria, idFieldName = 'id') {
    let result;
    if (criteria.cid) {
        result = criteria.cid === entity.cid;
    }
    const criteriaId = criteria[idFieldName] || criteria.id;
    if (result !== false && criteriaId) {
        const entityId = entity[idFieldName] || entity.id;
        result = criteriaId === entityId;
    }
    return !!result;
}
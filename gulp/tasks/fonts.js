﻿var gulp = require('gulp');
var fonts = require('../config').fonts;

gulp.task('fonts', function() {
    gulp.src(fonts.src).pipe(gulp.dest(fonts.dest))
});
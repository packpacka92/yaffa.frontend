﻿var gulp    = require('gulp');
var eslint  = require('gulp-eslint');
var gulpif  = require('gulp-if');
var cache   = require('gulp-cached');
var config  = require('../config').jsStyle;

gulp.task('js-style', function () {
    return gulp.src(config.src)
		.pipe(cache('linting'))    	
        .pipe(eslint())
        .pipe(eslint.format());
});


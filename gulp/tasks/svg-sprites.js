var gulp = require('gulp');
var svgSprites = require('gulp-svg-sprite');
var svg = require('../config').svg;
var gutil = require('gulp-util');

gulp.task('svg-sprites', function() {
    var spriteConfig = {
        mode: {
            symbol: {
                dest: '.',
                sprite: 'svg-sprite.svg',
                example: true,
                render: {
                    less: true
                }
            }
        },
        shape: { transform: ['svgo'] }
    };

    return gulp.src(svg.src)
        .pipe(svgSprites(spriteConfig))
        .pipe(gulp.dest(svg.dest));
});